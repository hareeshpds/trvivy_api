from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
import subprocess
import json
from pathlib import Path
BASE_DIR = Path(__file__).resolve().parent.parent



class DockerImageScan(APIView):
    def get(self, request, format=None):
        """ scan image through trivy """

        #subprocess.run(["trivy","--reset"])

        # getting target through query parameter
        image_name = request.GET.get('target')
        image_name =image_name.replace('"','')
        # run trivy command 
        test = subprocess.run(["trivy","image","-f","json","-o","results.json",image_name])
        #load json data provided by trivy 
        file_path = BASE_DIR / 'results.json'
        with open(file_path) as f:
        	data=json.load(f)
        # Remove json file
        subprocess.run(["rm",file_path])
        return Response(data)
        
        
        

class RepoScan(APIView):
    def get(self, request, format=None):
        """ scan repo through trivy """

        #subprocess.run(["trivy","--reset"])
        repo_name = request.GET.get('target')
        repo_name = repo_name.replace('"','')
        test = subprocess.run(["trivy","repo","-f","json","-o","results1.json",repo_name])
         
        file_path = BASE_DIR / 'results1.json'
        with open(file_path) as f:
        	data=json.load(f)
        subprocess.run(["rm",file_path])
        return Response(data)
